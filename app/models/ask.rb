class Ask
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :title, type: String
  field :content, type: String
  field :date, type: String
  field :complete, type: Boolean
    
  field :comment_id, type: String    
  field :user_id, type: String
  field :user_email, type: String
  field :user_full_name, type: String

 # embeds_many :comments
  has_many :comments, class_name: 'Comment', inverse_of: :ask 
  belongs_to :user, class_name: 'User', inverse_of: :asks
  
    def self.search(search)
        if search
           Ask.any_of({ title: /#{search}/ })
        else
           scoped
        end
    end
  
end
