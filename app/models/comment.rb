class Comment
  include Mongoid::Document
  include Mongoid::Timestamps
  field :name, type: String
  field :content, type: String

  field :ask_id, type: String
  field :user_id, type: String
  field :user_email, type: String
  field :user_image, type: String

  #embedded_in :asks, :inverse_of => :comments
  belongs_to :ask, class_name: 'Ask', inverse_of: :comments
  belongs_to :user, class_name: 'User', inverse_of: :comments
end
