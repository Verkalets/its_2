class Post
  include Mongoid::Document
  include Mongoid::Timestamps

  field :title, type: String
  field :content, type: String

  field :user_id, type: String
  field :user_email, type: String
  field :imagep, type: String
  
  mount_uploader :imagep, ImagepUploader

  belongs_to :user, class_name: 'User', inverse_of: :posts
  
    def self.search(search)
        if search
           Post.any_of({ title: /#{search}/ })
        else
           scoped
        end
    end
  
end
