class User
  include Mongoid::Document
  include Mongoid::Timestamps
 # include User::AuthDefinitions

  after_create :assign_default_role
  has_many :identities
  has_many :asks, class_name: 'Ask', inverse_of: :user
  has_many :posts, class_name: 'Post', inverse_of: :user
  has_many :news, class_name: 'News', inverse_of: :user
  has_many :comments, class_name: 'Comment', inverse_of: :user

  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  ## Database authenticatable
  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""
  field :full_name , type: String, default: ""
  field :about , type: String, default: ""
  field :age , type: String, default: ""
  field :userspro, type: String 

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String
  
  
  
  mount_uploader :userspro, UsersproUploader



  def assign_default_role
    add_role(:user)
  end
  

  ## Confirmable
  # field :confirmation_token,   type: String
  # field :confirmed_at,         type: Time
  # field :confirmation_sent_at, type: Time
  # field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    type: String # Only if unlock strategy is :email or :both
  # field :locked_at,       type: Time
end
