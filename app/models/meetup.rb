class Meetup
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :title, type: String
  field :content, type: String
  field :excerpt, type: String
  field :date, type: String
  field :address, type: String
  field :imagem, type: String
  
  mount_uploader :imagem, ImagemUploader
  
    def self.search(search)
        if search
           Meetup.any_of({ title: /#{search}/ })
        else
           scoped
        end
    end
  
end
