class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    
    if user.has_role? :admin
      can :manage, :all
    elsif user.has_role? :user 
      can :read, Meetup
      can :read, News
      can [:create, :read], Ask
      can [:create, :read], Post
      can :manage, Ask, user_id: user.id
      can :manage, Comment, user_id: user.id
      can :manage, Post, user_id: user.id
    else
      can :read, :all #TODO Change user wirte       
    end
  end
end
