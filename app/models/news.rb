class News
  include Mongoid::Document
  include Mongoid::Timestamps

  resourcify

  field :title, type: String
  field :content, type: String
  field :user_id, type: String
  field :user_email, type: String
  field :imagen, type: String
  
  mount_uploader :imagen, ImagenUploader

  belongs_to :user, class_name: 'User', inverse_of: :news
  
    def self.search(search)
        if search
           News.any_of({ title: /#{search}/ })
        else
           scoped
      end
    end
  
end
