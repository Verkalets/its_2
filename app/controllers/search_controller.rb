class SearchController < ApplicationController
  def result
      @posts = Post.search(params[:search])
      @news = News.search(params[:search])
      @meetups = Meetup.search(params[:search])
      @asks = Ask.search(params[:search])
      @meetup = Meetup.last
  end
end
