class AsksController < ApplicationController
  before_action :set_ask, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource param_method: :my_sanitizer

  # GET /asks
  # GET /asks.json
  def index
    @asks = Ask.all.order_by(created_at: :desc).page(params[:page]).per(4)
    @user = current_user
    @meetup = Meetup.last
    @news = News.all.order_by(created_at: :desc).limit(3)
  end

  # GET /asks/1
  # GET /asks/1.json
  def show
    @meetup = Meetup.last
    @news = News.all.order_by(created_at: :desc).limit(3)
  end

  # GET /asks/new
  def new
    @ask = Ask.new
    @meetup = Meetup.last
    @news = News.all.order_by(created_at: :desc).limit(3)
  end

  # GET /asks/1/edit
  def edit
  end

  # POST /asks
  # POST /asks.json
  def create

    @ask = current_user.asks.new(ask_params)
    @ask.user_email = current_user.email
    @ask.user_full_name = current_user.full_name

    respond_to do |format|
      if @ask.save
        format.html { redirect_to @ask, notice: 'Ask was successfully created.' }
        format.json { render action: 'show', status: :created, location: @ask }
      else
        format.html { render action: 'new' }
        format.json { render json: @ask.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /asks/1
  # PATCH/PUT /asks/1.json
  def update
    respond_to do |format|
      if @ask.update(ask_params)
        format.html { redirect_to @ask, notice: 'Ask was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @ask.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /asks/1
  # DELETE /asks/1.json
  def destroy
    @ask.destroy
    respond_to do |format|
      format.html { redirect_to asks_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ask
      @ask = Ask.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ask_params
      params.require(:ask).permit(:title, :content, :user_email, :complete, :user_id)
    end

    def my_sanitizer
      params.require(:ask).permit(:name)
    end
end
