class CommentsController < ApplicationController
	load_and_authorize_resource param_method: :my_sanitizer

	def create
	  @ask = Ask.find(params[:ask_id])
	  @comment = @ask.comments.create!(comment_params)
	  @comment.user_email = current_user.email	
	  @comment.user_image = current_user.userspro	

	  if @comment.save
	  redirect_to @ask, :notice => "Comment created!"
	  else
		redirect_to @ask, :notice => "Error"
	  end	
	end


	private
    
    def comment_params
    	params.require(:comment).permit(:name, :content)
 	end

 	def my_sanitizer
      params.require(:comment).permit(:name)
    end

end
