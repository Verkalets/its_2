class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource param_method: :my_sanitizer

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.all.order_by(created_at: :desc).page(params[:page]).per(3)
    @news = News.all.order_by(created_at: :desc).limit(3)
    @meetup = Meetup.last

     @user = current_user

    if @user.nil?
      @user = User.new
    else
      @user = current_user
    end
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @meetup = Meetup.last
    @news = News.all.order_by(created_at: :desc).limit(3)
  end

  # GET /posts/new
  def new
    @post = Post.new
    @posts = Post.all.order_by(created_at: :desc).page(params[:page]).per(3)
    @meetup = Meetup.last
    @news = News.all.order_by(created_at: :desc).limit(3)
  end

  # GET /posts/1/edit
  def edit
    @meetup = Meetup.last
    @news = News.all
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = current_user.posts.new(post_params)
    @post.user_email = current_user.email
    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render action: 'show', status: :created, location: @post }
      else
        format.html { render action: 'new' }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:title, :content, :imagep)
    end

    def my_sanitizer
      params.require(:post).permit(:name)
    end
end
