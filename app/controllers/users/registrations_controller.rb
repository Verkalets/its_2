class Users::RegistrationsController < Devise::RegistrationsController
  
  before_filter :configure_permitted_parameters
  
  private
  
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) do |u|
      u.permit(:full_name, :age,:userspro,:about,
        :email, :password, :password_confirmation)
    end
    
    
    devise_parameter_sanitizer.for(:account_update) do |u|
      u.permit(:full_name,:userspro,:about,
        :age, :password, :password_confirmation, :current_password)
    end
  end
  
  private :resource_params
end