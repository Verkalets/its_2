class Users::PasswordsController < Devise::PasswordsController
  def resource_params
    params.require(:user).permit(:age,:full_name, :about, :userspro, :email, :password, :password_confirmation, :reset_password_token)
  end
  private :resource_params
end