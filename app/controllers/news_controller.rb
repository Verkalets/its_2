class NewsController < ApplicationController
  before_action :authenticate_user! , only: [:edit, :update, :destroy]
  before_action :set_news, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource param_method: :my_sanitizer


  # GET /news
  # GET /news.json
  def index
    @news = News.all.order_by(created_at: :desc).page(params[:page]).per(3)
    @news_other = News.all.order_by(created_at: :desc).limit(5)
    @posts = Post.all
    @meetup = Meetup.last

    @user = current_user

    if @user.nil?
      @user = User.new
    else
      @user = current_user
    end
  end

  # GET /news/1
  # GET /news/1.json
  def show
    @user = current_user
    @meetup = Meetup.last
    @news_all = News.all.order_by(created_at: :desc).limit(3)
  end

  # GET /news/new
  def new
    @news = News.new
    @meetup = Meetup.last
    @news_all = News.all.order_by(created_at: :desc).limit(3)
  end

  # GET /news/1/edit
  def edit
    @meetup = Meetup.last
    @news = News.all.order_by(created_at: :desc).page(params[:page]).per(3)
  end

  # POST /news
  # POST /news.json
  def create
    @news = News.new(news_params)

    respond_to do |format|
      if @news.save
        format.html { redirect_to @news, notice: 'News was successfully created.' }
        format.json { render action: 'show', status: :created, location: @news }
      else
        format.html { render action: 'new' }
        format.json { render json: @news.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /news/1
  # PATCH/PUT /news/1.json
  def update
    respond_to do |format|
      if @news.update(news_params)
        format.html { redirect_to @news, notice: 'News was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @news.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /news/1
  # DELETE /news/1.json
  def destroy
    @news.destroy
    respond_to do |format|
      format.html { redirect_to news_index_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_news
      @news = News.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def news_params
      params.require(:news).permit(:title, :content, :imagen)
    end

    def my_sanitizer
      params.require(:news).permit(:name)
    end
end
