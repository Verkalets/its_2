class IndexController < ApplicationController
  
  def home
      @news = News.all.order_by(created_at: :desc).limit(3)
      @posts = Post.all.order_by(created_at: :desc).page(params[:page]).per(3)
      @meetup = Meetup.last
      @news_last = News.last
  end
end
