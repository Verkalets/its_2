json.array!(@meetups) do |meetup|
  json.extract! meetup, :id, :title, :content, :excerpt, :date, :address
  json.url meetup_url(meetup, format: :json)
end
