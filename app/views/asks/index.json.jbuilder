json.array!(@asks) do |ask|
  json.extract! ask, :id, :title, :content
  json.url ask_url(ask, format: :json)
end
